﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class Character : MonoBehaviour {

	public enum CharacterPosition { FRONT, BACK };

	public CharacterClass Class;
	public CharacterStats Stats;
	public CharacterPosition Position;

	private Animator _animator;

	public void Start()
	{
		_animator = this.GetComponent<Animator>();
	}

	public void FixedUpdate()
	{
		if (Input.GetKey(KeyCode.Space))
		{
			_animator.SetTrigger("attack");
		}
	}

	public void Flip()
	{
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		// _isFacingRight = transform.localScale.x > 0;
	}
}
