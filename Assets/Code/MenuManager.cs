﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public void ChangeToScene (string newScene ) {
		Application.LoadLevel(newScene);
	}

	public void QuitGame() {
		Application.Quit();
	}
}
