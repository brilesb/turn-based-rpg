﻿using System;
using System.Collections;

[Serializable]
public class CharacterStats {

	public int HP;
	public int MP;
	public int AttackPower;
	public int MagicPower;
	public int PhysicalDefense;
	public int MagicDefense;
	public int Speed;
}
