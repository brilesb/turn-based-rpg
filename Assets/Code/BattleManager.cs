﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class BattleManager : MonoBehaviour {

	public enum BattleState {
		START,
		PLAYER_CHOICE,
		ENEMY_CHOICE,
		DO_ACTION,
		WIN,
		LOSE
	}

	private BattleState _currentState;
	private List<BattleAction> _actions;

	public List<GameObject> _playerPrefabs;
	public List<GameObject> _enemyPrefabs;

	void Start () {
		_currentState = BattleState.START;

		InitPlayerCharacters();
		InitEnemyCharacters();
	}

	private void InitPlayerCharacters() {

		float spacing = 1f;

		Vector3 position = new Vector3(0,3f);
		float frontX = 1.3f;
		float backX = 1.75f;

		foreach(var prefab in _playerPrefabs) {

			// Determine if this character is in front or back
			Character character = prefab.GetComponent<Character>();
			if(character.Position == Character.CharacterPosition.BACK)
				position.x = backX;
			else
				position.x = frontX;

			Instantiate(prefab, position, Quaternion.identity );
			position.y -= spacing;
		}
	}

	private void InitEnemyCharacters() {
		
		float spacing = 1f;
		
		Vector3 position = new Vector3(0,3f);
		float frontX = -1.3f;
		float backX = -1.75f;
		
		foreach(var prefab in _enemyPrefabs) {
			
			// Determine if this character is in front or back
			Character character = prefab.GetComponent<Character>();
			if(character.Position == Character.CharacterPosition.BACK)
				position.x = backX;
			else
				position.x = frontX;

			// flip them and tint them so they are recognizable as an enemy
			character.Flip();
			
			Instantiate(prefab, position, Quaternion.identity );
			position.y -= spacing;
		}
	}


	void Update () {
		
	}


}
